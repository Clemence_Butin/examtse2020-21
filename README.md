# Coffeebar Simulation

#### Authors
* Cl�mence BUTIN - 21910436
* Nikita MARINI - 21900525

####Description of the exercise
This project is aimed at simulating and analyzing five years of operations of a fictional 
coffeebar, where some of the parameters of the simulation (such as customers'purchasing decisions) are
derived from a pre-existing dataset, while others (such as the characteristics of the customers)
are defined in accordance to the instructions contained the Documentation folder.

####Structure of the project
Four folders can be found inside the project. The folder *Data* contains the 
departing dataset for the analysis, consisting of a 5-year simulation of a
coffeeshop. This data is analyzed in the script *Exploratory.py* in the folder *Code*.
Most notably, this script contains the code needed to calculate the probabilities 
that at any given time a customer would make a given purchase (i.e., that he will 
choose a certain drink and a certain food at the time he walks into the 
store). The final dataframe displaying all the probabilities can be found in the 
folder *Results*, under the name *Probs.csv*. 
Additionally, the *Code* folder contains three more scripts: 
* Customers.py defines the classes (that is, the customers), by giving to each one 
of them specific attributes and methods that will be needed in the Simulation.
* Simulation.py creates the pool of customers and simulates the five years of operation
of the bar, and contains a quick analysis of the result by means of graphical representation. 
* Additional_questions.py explores some additional scenarios, that are describe on Slide 12 of 
the PPT document contained in *Documentation*. 

Lastly, the folder *Results* contains, in addition to what mentioned above, the results of 
the original Simulation (filename: *Simulation.csv*), those of 
the simulations discussed in *Additional_questions.py*, (filename: *Simulation1.csv*, 
*Simulation2.csv, ...), as well as all the graphs that are generated when 
analysing the results of the simulations.  

####Order of execution 
If the user seeks to reperform the entirety of the exercise, the order of execution of the 
scripts is the following: 
1) Exploratory.py
2) Customers.py
3) Simulations.py
4. Additional_questions.py

Alternatively, if only parts of the excercise are of interest, paths to import all of the 
necessary data are provided at the beginning of each script.  