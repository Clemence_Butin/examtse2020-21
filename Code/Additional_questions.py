########################################################
############# TSE M2 EEE: Python Programming ###########
############## Clémence Butin 21910436 #################
##############  Nikita Marini 21900525 #################
########################################################

import datetime
import os
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

# Declare working directory to use Pycharm console
# os.chdir("/Users/NikJM/Desktop/Uni/Python/examtse2020-21/Code")
# os.chdir("/Users/clemencebutin/PycharmProjects/examtse2020-21/Data")

importpath = os.path.abspath(("../Data/Coffeebar_2016-2020.csv"))
df = pd.read_csv(importpath, sep=";")
probs = pd.read_csv("../Results/Probs.csv", sep=";")
Simulation = pd.read_csv("../Results/Simulation.csv", sep=";")
Simulation1 = pd.read_csv("../Results/Simulation1.csv", sep=";")  # Number of returning customers drops to 50
Simulation2 = pd.read_csv("../Results/Simulation2.csv", sep=";")  # Prices go up by 20% at the beginning of the third
# year of simulation
Simulation3 = pd.read_csv("../Results/Simulation3.csv", sep=";")  # Budget of hipsters drops to 40
Simulation4 = pd.read_csv("../Results/Simulation4.csv", sep=";")  # Put uniform probabilities

###############################################
################### PART 4 ####################
##################### Q1 ######################
###Show some buying histories of returning#####
##customers for your simulations###############
###############################################

cust = Simulation['Customer_ID'].value_counts()  # Number of time a customer comes at the coffee place
returning_cust = cust[cust > 1]  # 1000 Returning customers

print(Simulation[Simulation['Customer_ID'] == "CID10878626"])  # Show the history of one specific customer
print(Simulation[Simulation['Customer_ID'] == "CID10205827"])

###############################################
################### PART 4 ####################
##################### Q2 ######################
###############################################
# In the provided data set (Coffeebar_2016-2020.csv) there are actual returning customers.
# How many? Do they have specific times when they show up more? Can you determine a probability of having a
# onetime or returning customer at a given time? How does this impact their buying history? Do you see correlations
# between what returning customers buy and one-timers?

# Analysis of the original dataset and returning customers in there
Customers = df['CUSTOMER'].value_counts()
ReturningCustomers = Customers[Customers > 1]
print(len(ReturningCustomers))  ###There are 1000 returning
UniqueCustomers = Customers[Customers == 1]
print(len(UniqueCustomers))

# Leftjoin to obtain the number of times each customer showed up as a new column
CustomersMerge = pd.merge(df, Customers, how='left', left_on=df['CUSTOMER'], right_on=Customers.index)
# Keep only Returning customers (count >1)
ReturningCount = CustomersMerge[CustomersMerge['CUSTOMER_y'] != 1]
# Remove duplicates that were there by construction (we started from the whole dataset)
RetuningCount2 = ReturningCount.drop_duplicates(subset=['key_0'])
# Create hour and day variables
RetuningCount2['HOUR'] = df['TIME'].apply(lambda strtime: datetime.datetime.strptime(strtime, '%Y-%m-%d %H:%M:%S').hour)
RetuningCount2['DAY'] = df['TIME'].apply(lambda strtime: datetime.datetime.strptime(strtime, '%Y-%m-%d %H:%M:%S').day)
RetuningCount2['TIMESTAMP'] = df['TIME'].apply(
    lambda strtime: str(datetime.datetime.strptime(strtime, '%Y-%m-%d %H:%M:%S').strftime('%H:%M')))

# Count number of occurrences per hour and per day, to check for eventual trends
HourCount = RetuningCount2[['HOUR', 'key_0']].groupby('HOUR').count()
DayCount = RetuningCount2[['DAY', 'key_0']].groupby('DAY').count()
HourCount = HourCount.sort_values(['key_0'], ascending=[False])
DayCount = DayCount.sort_values(['key_0'], ascending=[False])

CustomersMerge['TIMESTAMP'] = df['TIME'].apply(
    lambda strtime: str(datetime.datetime.strptime(strtime, '%Y-%m-%d %H:%M:%S').strftime('%H:%M')))
CustomersMerge['RETURNING'] = ""
CustomersMerge.loc[CustomersMerge['CUSTOMER_y'] == 1, 'RETURNING'] = 'Onetimer'
CustomersMerge.loc[CustomersMerge['CUSTOMER_y'] > 1, 'RETURNING'] = 'Returning'

ReturningTime = []
for time in np.unique(CustomersMerge['TIMESTAMP']):
    print(time)
    ProbReturning = CustomersMerge[CustomersMerge['TIMESTAMP'] == time]['RETURNING'].value_counts(
        normalize=True).rename(time)
    ReturningTime.append(ProbReturning)
print(ReturningTime)

ProbTypes = pd.DataFrame(ReturningTime)
print(ProbTypes)

# Show graphically the probability of onetimers vs returning on one graph for each timestamp
plt.plot(ProbTypes.index, ProbTypes['Onetimer'], color='blue', label="Onetimers")
plt.plot(ProbTypes.index, ProbTypes['Returning'], color='red', label="Returning")
plt.xticks(ProbTypes.index[::10], ProbTypes.index[::10])
plt.legend(prop={'size': 8})
plt.title("Type of customer's probability across the day")
plt.show()
#plt.savefig("../Results/Graphs/P4_QU2_Proba.png", bbox_inches='tight')

# Correlation between buying trends and returning/onetime customers
# Create df's with returning only and onetimer only
ReturningOnly = CustomersMerge[CustomersMerge['RETURNING'] == 'Returning']
OnetimerOnly = CustomersMerge[CustomersMerge['RETURNING'] == 'Onetimer']

# RETURNING ONLY
# Calculate probability of drinks
ReturningDrinks = []
for time in np.unique(ReturningOnly['TIMESTAMP']):
    print(time)
    TempDF = ReturningOnly[ReturningOnly['TIMESTAMP'] == time]['DRINKS'].value_counts(normalize=True).rename(time)
    ReturningDrinks.append(TempDF)
pd.concat(ReturningDrinks, axis=1)

# Calculate probability of food
ReturningFood = []
for time in np.unique(ReturningOnly['TIMESTAMP']):
    print(time)
    TempDF = ReturningOnly[ReturningOnly['TIMESTAMP'] == time]['FOOD'].value_counts(normalize=True,
                                                                                    dropna=False).rename(time)
    ReturningFood.append(TempDF)
pd.concat(ReturningFood, axis=1)

# Turn them into df's
ReturningDrinksDF = pd.DataFrame(ReturningDrinks)
print(ReturningDrinksDF)
ReturningFoodDF = pd.DataFrame(ReturningFood)
ReturningFoodDF = ReturningFoodDF.fillna(0)
ReturningFoodDF.columns = ReturningFoodDF.columns.fillna('none')
print(ReturningFoodDF)
# Merge drinks and food
ReturningProbs = pd.merge(ReturningDrinksDF, ReturningFoodDF, how='left', left_index=True, right_index=True)
print(ReturningProbs)

# ONETIME ONLY
# Calculate probability of drinks
OnetimeDrinks = []
for time in np.unique(OnetimerOnly['TIMESTAMP']):
    print(time)
    TempDF = OnetimerOnly[OnetimerOnly['TIMESTAMP'] == time]['DRINKS'].value_counts(normalize=True).rename(time)
    OnetimeDrinks.append(TempDF)
pd.concat(OnetimeDrinks, axis=1)

OnetimeFood = []
for time in np.unique(OnetimerOnly['TIMESTAMP']):
    print(time)
    TempDF = OnetimerOnly[OnetimerOnly['TIMESTAMP'] == time]['FOOD'].value_counts(normalize=True, dropna=False).rename(
        time)
    OnetimeFood.append(TempDF)
pd.concat(OnetimeFood, axis=1)

OnetimeDrinksDF = pd.DataFrame(OnetimeDrinks)
print(OnetimeDrinksDF)
OnetimeFoodDF = pd.DataFrame(OnetimeFood)
OnetimeFoodDF = OnetimeFoodDF.fillna(0)
OnetimeFoodDF.columns = OnetimeFoodDF.columns.fillna('none')
print(OnetimeFoodDF)

OnetimerProbs = pd.merge(OnetimeDrinksDF, OnetimeFoodDF, how='left', left_index=True, right_index=True)
print(OnetimerProbs)

# Take one food/drink randomly and show how it differs across returning and onetimers
plt.plot(OnetimeDrinksDF.index, OnetimeDrinksDF['coffee'], color='sienna')
plt.plot(ReturningDrinksDF.index, ReturningDrinksDF['coffee'], color='goldenrod')
plt.xticks(OnetimeDrinksDF.index[::10], OnetimeDrinksDF.index[::10])
plt.legend(['Onetimers', 'Returning'], loc='upper right')
plt.title('Probability of buying a coffee throughout the day')
plt.xlabel('Time of the day')
plt.ylabel('Probability')
#plt.show()

plt.plot(OnetimeFoodDF.index, OnetimeFoodDF['cookie'], color='sienna')
plt.plot(ReturningFoodDF.index, ReturningFoodDF['cookie'], color='goldenrod')
plt.xticks(OnetimeFoodDF.index[::10], OnetimeFoodDF.index[::10])
plt.legend(['Onetimers', 'Returning'], loc='lower right')
plt.title('Probability of buying a cookie throughout the day')
plt.xlabel('Time of the day')
plt.ylabel('Probability')
#plt.show()

###############################################
################### PART 4 ####################
##################### Q3 ######################
###What would happen if we lower the returning#
# customers to 50 and simulate the same period?#
###############################################
Simulation1.replace(r'\s+', np.nan, regex=True)  # Replacing the blank with NaN
Simulation1.fillna(0)  # Replacing the NaN with 0

Customer = Simulation1['Customer_ID'].value_counts()
ReturningCustomer = Customer[Customer > 1]
print(len(ReturningCustomer))  # 50 returning customers
UniqueCustomer = Customer[Customer == 1]
print(len(UniqueCustomer))  # 249 814 unique customers

# Total income across years
Simulation1['Year'] = Simulation1['Timestamp'].apply(
    lambda strtime: datetime.datetime.strptime(strtime, '%Y-%m-%d %H:%M:%S').year)
Simulation['Year'] = Simulation['Timestamp'].apply(
    lambda strtime: datetime.datetime.strptime(strtime, '%Y-%m-%d %H:%M:%S').year)
income_time = Simulation[['Income', 'Year']].groupby('Year').sum()
income_year1 = Simulation1[['Income', 'Year']].groupby('Year').sum()

plt.plot(income_time, color='olive', linewidth=2, label="1000 returning customers")
plt.plot(income_year1, color='skyblue', linewidth=2, label="50 returning customers")
plt.xticks(np.arange(2021, 2026, 1))
plt.xlabel('Years')
plt.ylabel("Income")
plt.title('Income across years')
plt.legend(prop={'size': 8})
# plt.savefig("../Results/Graphs/P4_QU3.png", bbox_inches='tight')

###############################################
################### PART 4 ####################
##################### Q4 ######################
###The prices change from the beginning of#####
##########2018 and go up by 20%################
###############################################

# Total income across years
Simulation2['Year'] = Simulation2['Timestamp'].apply(
    lambda strtime: datetime.datetime.strptime(strtime, '%Y-%m-%d %H:%M:%S').year)
income_time2 = Simulation2[['Income', 'Year']].groupby('Year').sum()

plt.plot(income_time2, color='skyblue', linewidth=2, label="With inflation")
plt.plot(income_time, color='olive', linewidth=2, label="Without inflation")
plt.xticks(np.arange(2021, 2026, 1))
plt.xlabel('Years')
plt.ylabel("Income")
plt.ylim(300000, 370000)
plt.title('Income across years')
plt.legend()
# plt.savefig("../Results/Graphs/P4_QU4.png", bbox_inches='tight')

###############################################
################### PART 4 ####################
##################### Q5 ######################
#######The budget of hipsters drops to 40######
###############################################

# Total income across years
Simulation3.replace(r'\s+', np.nan, regex=True)  # Replacing the blank with NaN
Simulation3.fillna(0)  # Replacing the NaN with 0

Simulation3['Year'] = Simulation3['Timestamp'].apply(
    lambda strtime: datetime.datetime.strptime(strtime, '%Y-%m-%d %H:%M:%S').year)
income1 = Simulation3[['Income', 'Year']].groupby('Year').sum()

plt.plot(income1, color='skyblue', label="Budget to 40")
plt.plot(income_time, color='red', label="Budget to 500")
plt.xticks(np.arange(2021, 2026, 1))
plt.xlabel('Years')
plt.ylabel("Income")
plt.title('Income across years')
plt.legend()
#plt.savefig("../Results/P4_QU5.png", bbox_inches='tight')

###############################################
################### PART 4 ####################
##################### Q6 ######################
########## Put uniform probabilities ##########
###############################################
#Creating new columns to facilitate the comparaison between the two simulations
Simulation['Year'] = pd.DatetimeIndex(Simulation['Timestamp']).year
Simulation['Month'] = pd.DatetimeIndex(Simulation['Timestamp']).month
Simulation['Day'] = pd.DatetimeIndex(Simulation['Timestamp']).day
Simulation['Hour'] = pd.DatetimeIndex(Simulation['Timestamp']).hour
Simulation['Minute'] = pd.DatetimeIndex(Simulation['Timestamp']).minute
Simulation['BusinessDay'] = pd.to_datetime(Simulation[['Year', 'Month', 'Day']])

Simulation4['Year'] = pd.DatetimeIndex(Simulation4['Timestamp']).year
Simulation4['Month'] = pd.DatetimeIndex(Simulation4['Timestamp']).month
Simulation4['Day'] = pd.DatetimeIndex(Simulation4['Timestamp']).day
Simulation4['Hour'] = pd.DatetimeIndex(Simulation4['Timestamp']).hour
Simulation4['Minute'] = pd.DatetimeIndex(Simulation4['Timestamp']).minute
Simulation4['BusinessDay'] = pd.to_datetime(Simulation4[['Year', 'Month', 'Day']])

# Show trends in consumption by year
#Simulation['Food'] = Simulation['Food'].replace("none", None)
YearlyCountFood1 = Simulation4[Simulation4['Food'] != 'none'][['Food', 'Year']].groupby('Year').count()
YearlyCountDrink1 = Simulation4[['Drink', 'Year']].groupby('Year').count()
YearlyCount1 = pd.merge(YearlyCountFood1, YearlyCountDrink1, on = 'Year')
YearlyCount1['Year'] = YearlyCount1.index
YearlyCount1.plot(x='Year', y=['Drink', 'Food'], kind="bar")
plt.title('Food and drinks bought per year')
#plt.show()

# Show trends in consumption by month
MonthlyCountFood1 = Simulation4[Simulation4['Food'] != 'none'][['Food', 'Month']].groupby('Month').count()
MonthlyCountDrink1 = Simulation4[['Drink', 'Month']].groupby('Month').count()
MonthlyCount1 = pd.merge(MonthlyCountFood1, MonthlyCountDrink1, on = 'Month', right_index = True)
MonthlyCount1['Month'] = MonthlyCount1.index
MonthlyCount1.plot(x='Month', y=['Drink', 'Food'], kind = 'bar')
plt.title('Food and drinks bought per month')
#plt.show()


#Income accross years
income_QU6 = Simulation4[['Income', 'Year']].groupby('Year').sum()

plt.plot(income_QU6, color='skyblue', label="Budget to 40")
#plt.plot(income_time, color='red', label="Budget to 500")
plt.xticks(np.arange(2021, 2026, 1))
plt.xlabel('Years')
plt.ylabel("Income")
plt.title('Income across years')
#plt.legend()
#plt.savefig("../Results/P4_QU5.png", bbox_inches='tight')

# Average income during the day Simulation4
Income_avg = []
for hour in np.unique(Simulation4['Hour']):
    print(hour)
    Temp = Simulation4[Simulation4['Hour'] == hour][['Income', 'Hour']].groupby('Hour').sum() / len(
        np.unique(Simulation4['BusinessDay']))
    Income_avg.append(Temp)
Income_avg = pd.concat(Income_avg, axis=0)

# Average income during the day Simulation
IncomeTime = []
for hour in np.unique(Simulation['Hour']):
    print(hour)
    Temp = Simulation[Simulation['Hour'] == hour][['Income', 'Hour']].groupby('Hour').sum() / len(
        np.unique(Simulation['BusinessDay']))
    IncomeTime.append(Temp)
IncomeTime = pd.concat(IncomeTime, axis=0)

concat = pd.concat([IncomeTime,Income_avg], axis=1)
concat.columns = ['Income - Normal proba', 'Income - Uniform proba']

concat.plot.bar()
plt.ylabel("Income")
plt.title('Average income during the day')
plt.legend(prop={'size': 8})

