########################################################
############# TSE M2 EEE: Python Programming ###########
############## Clémence Butin 21910436 #################
##############  Nikita Marini 21900525 #################
########################################################

import numpy as np
import os
import pandas as pd
import random
import time
import matplotlib.pyplot as plt
from Code import Customers
from Customers import *

# Declare working directory to use Pycharm console
# os.chdir("/Users/NikJM/Desktop/Uni/Python/examtse2020-21/Code")
# os.chdir("/Users/clemencebutin/PycharmProjects/examtse2020-21/Data")

importpath = os.path.abspath(("../Data/Coffeebar_2016-2020.csv"))
df = pd.read_csv(importpath, sep=";")
probs = pd.read_csv("../Results/Probs.csv", sep=";")

# Create big big dataframe that will contain the information recorded by the coffee shop
# Time goes from 08:00 01-01-2021 to 17:56 31-12-2025
Simulation = pd.DataFrame()
Simulation['Timestamp'] = pd.to_datetime(df['TIME']) + pd.offsets.DateOffset(years=5)
Simulation['Time'] = Simulation['Timestamp'].dt.strftime('%H:%M')
Simulation['Year'] = pd.DatetimeIndex(Simulation['Timestamp']).year.astype(int)
Simulation['Customer_ID'] = ""
Simulation['Drink'] = ""
Simulation['Food'] = ""
Simulation['Income'] = ""
print(Simulation)

# List of classes
Classes = ["Returning", "Onetimers"]
Subclasses = ["Returning_regular", "Hipster", "Onetimer_regular", "Tripadvisor"]

# Create IDs of Returning Customers
random.seed(4628)
Range_returning = range(10000000, 11000000)  # range of 1,000,000 values to be assigned to returning fellas
ReturningCustomers_IDs = random.sample(Range_returning, 1000)  # sample 1,000 of those values for the returning that "exist"
for i in range(len(ReturningCustomers_IDs)):
    ReturningCustomers_IDs[i] = "CID" + str(ReturningCustomers_IDs[i])

# Define types of the returning customers
ReturningTypes = list(np.repeat(Subclasses[0], 667)) + list(np.repeat(Subclasses[1], 333))

# Create list of objects -- returning customers
ReturningCustomers = []
for enum, type_cust in enumerate(ReturningTypes):
    if type_cust == "Returning_regular":
        Customer = Returning_regular(ReturningCustomers_IDs[enum])  # Assign an ID based on the index
        ReturningCustomers.append(Customer)
    elif type_cust == "Hipster":
        Customer = Hipster(ReturningCustomers_IDs[enum])
        ReturningCustomers.append(Customer)

# Simulate
random.seed(2425)
start = time.time()
ID_onetimers = 11000000  # Initialize the first ID for a onetimer, to which we're gonna add one in order to make sure
# he won't show up again!
deleted_customers = []

for ind in range(Simulation.shape[0]):
    print(ind)
    SelectedClass = np.random.choice(Classes, p=[0.2, 0.8])
    if (SelectedClass == 'Returning') & (len(ReturningCustomers) != 0):
        # Need to make sure there are returning customers from which to draw from
        SelectedCustomer = random.choices(list(enumerate(ReturningCustomers)),
                                          weights=np.repeat(1 / len(ReturningCustomers), len(ReturningCustomers)), k=1)
        # Indexes needed to remove him if he goes bust
        Customer = SelectedCustomer[0][1]
        IndexCustomer = SelectedCustomer[0][0]
        if Customer.class_name == 'Returning_regular':
            if Customer.budget < (max(menufood.values()) + max(menudrink.values())):
                # We delete customers who went bust, so if they rock up again something must be wrong with our code
                print('I SHOULDN\'T BE HERE!')
                break
            else:
                Customer.purchase(Simulation['Time'].iloc[ind], Simulation['Year'].iloc[ind], probs)
                Customer.tell_drink(Simulation['Time'].iloc[ind])
                Customer.tell_food(Simulation['Time'].iloc[ind])
                Customer.tell_expense()
                Simulation["Customer_ID"].iloc[ind] = Customer.ID
                Simulation['Drink'].iloc[ind] = Customer.drink
                Simulation['Food'].iloc[ind] = Customer.food
                Simulation['Income'].iloc[ind] = Customer.expense
                Customer.food = 'none'
                Customer.drink = 'none'

        elif Customer.class_name == 'Hipster':
            if Customer.budget < (max(menufood.values()) + max(menudrink.values())):
                print('I SHOULDN\'T BE HERE!')
                break
            else:
                Customer.purchase(Simulation['Time'].iloc[ind], Simulation['Year'].iloc[ind], probs)
                Customer.tell_drink(Simulation['Time'].iloc[ind])
                Customer.tell_food(Simulation['Time'].iloc[ind])
                Customer.tell_expense()
                Simulation["Customer_ID"].iloc[ind] = Customer.ID
                Simulation['Drink'].iloc[ind] = Customer.drink
                Simulation['Food'].iloc[ind] = Customer.food
                Simulation['Income'].iloc[ind] = Customer.expense
                Customer.food = 'none'
                Customer.drink = 'none'

        # We now check whether the customer will be able to make a purchase in the next iteration;
        # if not, we remove him from the list
        if Customer.budget < (max(menufood.values()) + max(menudrink.values())):
            deleted_customers.append(ReturningCustomers[IndexCustomer])
            del ReturningCustomers[IndexCustomer]
            print(len(ReturningCustomers))
            print('Our customer is facing some financial constraints. I don\'t think we will see him for a while...')

    elif SelectedClass == 'Onetimers':
        SelectedType = np.random.choice(Subclasses[2:4], p=[9 / 10, 1 / 10])

        if SelectedType == 'Onetimer_regular':
            TempID = "CID" + str(ID_onetimers)  # Temporary ID
            Customer = Onetimer_regular(TempID)  # We create a onetimer
            Customer.purchase(Simulation['Time'].iloc[ind], Simulation['Year'].iloc[ind], probs)
            Customer.tell_drink(Simulation['Time'].iloc[ind])
            Customer.tell_food(Simulation['Time'].iloc[ind])
            Customer.tell_expense()
            Simulation["Customer_ID"].iloc[ind] = Customer.ID
            Simulation['Drink'].iloc[ind] = Customer.drink
            Simulation['Food'].iloc[ind] = Customer.food
            Simulation['Income'].iloc[ind] = Customer.expense

        elif SelectedType == 'Tripadvisor':
            TempID = "CID" + str(ID_onetimers)
            Customer = Tripadvisor(TempID)
            Customer.purchase(Simulation['Time'].iloc[ind], Simulation['Year'].iloc[ind], probs)
            Customer.tell_drink(Simulation['Time'].iloc[ind])
            Customer.tell_food(Simulation['Time'].iloc[ind])
            Customer.give_tips()
            Customer.tell_expense()
            Simulation["Customer_ID"].iloc[ind] = Customer.ID
            Simulation['Drink'].iloc[ind] = Customer.drink
            Simulation['Food'].iloc[ind] = Customer.food
            Simulation['Income'].iloc[ind] = Customer.expense

        ID_onetimers = ID_onetimers + 1  # Add one so we won't repeat onetimers

print(Simulation)
end = time.time()
print(end - start)

Simulation.to_csv("../Results/Simulation.csv", sep=";", index=False)

#### We draw some graphs in order to analyse some of the features of our Simulation
# The result of the Simulation is stored in the results folder: if it hasn't been run it can be imported via:
Simulation = pd.read_csv("../Results/Simulation.csv", sep = ";")

# Creating time variables to facilitate analysis
Simulation['Year'] = pd.DatetimeIndex(Simulation['Timestamp']).year
Simulation['Month'] = pd.DatetimeIndex(Simulation['Timestamp']).month
Simulation['Day'] = pd.DatetimeIndex(Simulation['Timestamp']).day
Simulation['Hour'] = pd.DatetimeIndex(Simulation['Timestamp']).hour
Simulation['Minute'] = pd.DatetimeIndex(Simulation['Timestamp']).minute
Simulation['BusinessDay'] = pd.to_datetime(Simulation[['Year', 'Month', 'Day']])

# Show trends in consumption by year
#Simulation['Food'] = Simulation['Food'].replace("none", None)
YearlyCountFood = Simulation[Simulation['Food'] != 'none'][['Food', 'Year']].groupby('Year').count()
YearlyCountDrink = Simulation[['Drink', 'Year']].groupby('Year').count()
YearlyCount = pd.merge(YearlyCountFood, YearlyCountDrink, on = 'Year')
YearlyCount['Year'] = YearlyCount.index
YearlyCount.plot(x='Year', y=['Drink', 'Food'], kind="bar")
plt.title('Food and drinks bought per year')
plt.show()
#plt.savefig("../Results/Graphs/P3_Year_ConsumptionFD.png", bbox_inches='tight')

# Show trends in consumption by month
MonthlyCountFood = Simulation[Simulation['Food'] != 'none'][['Food', 'Month']].groupby('Month').count()
MonthlyCountDrink = Simulation[['Drink', 'Month']].groupby('Month').count()
MonthlyCount = pd.merge(MonthlyCountFood, MonthlyCountDrink, on = 'Month', right_index = True)
MonthlyCount['Month'] = MonthlyCount.index
MonthlyCount.plot(x='Month', y=['Drink', 'Food'], kind = 'bar')
plt.title('Food and drinks bought per month')
plt.show()
#plt.savefig("../Results/Graphs/P3_Month_ConsumptionFD.png", bbox_inches='tight')

# Average income during the day
IncomeTime = []
for hour in np.unique(Simulation['Hour']):
    print(hour)
    Temp = Simulation[Simulation['Hour'] == hour][['Income', 'Hour']].groupby('Hour').sum() / len(
        np.unique(Simulation['BusinessDay']))
    IncomeTime.append(Temp)
IncomeTime = pd.concat(IncomeTime, axis=0)

IncomeTime.plot.bar()
plt.ylabel("Income")
plt.title('Average income during the day')
for i, v in enumerate(round(IncomeTime['Income'],2)):
    plt.text(IncomeTime.index[i] - 8.35, v + 0.05, str(v))
#plt.savefig("../Results/P3_Income_avg.png", bbox_inches='tight')

# Show trends of some items throughout the day
DrinkTime = Simulation.groupby(['Time', 'Drink']).size().to_frame('count').reset_index()
FoodTime = Simulation.groupby(['Time', 'Food']).size().to_frame('count').reset_index()

# Show total numbers of coffees sold
plt.stem(DrinkTime[DrinkTime['Drink'] == 'coffee']['Time'], DrinkTime[DrinkTime['Drink'] == 'coffee']['count'])
plt.xticks(DrinkTime[DrinkTime['Drink'] == 'coffee']['Time'][::10],
           DrinkTime[DrinkTime['Drink'] == 'coffee']['Time'][::10])
plt.title('Total number of coffee sold per time of the day (2021-2025)')
plt.xlabel('Time of the day')
plt.ylabel('Total number of coffees')
plt.show()

# Show total numbers of sanwiches sold
plt.stem(FoodTime[FoodTime['Food'] == 'sandwich']['Time'], FoodTime[FoodTime['Food'] == 'sandwich']['count'])
plt.xticks(FoodTime[FoodTime['Food'] == 'sandwich']['Time'][::10],
           FoodTime[FoodTime['Food'] == 'sandwich']['Time'][::10])
plt.title('Total number of sandwiches sold per time of the day (2021-2025)')
plt.xlabel('Time of the day')
plt.ylabel('Total number of sandwiches')
plt.show()
#plt.savefig("../Results/P3_Nb_Sandwich.png", bbox_inches='tight')
