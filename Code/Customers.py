########################################################
############# TSE M2 EEE: Python Programming ###########
############## Clémence Butin 21910436 #################
##############  Nikita Marini 21900525 #################
########################################################

import numpy as np
import os
import pandas as pd
from random import uniform
from random import seed
from random import randint

# Declare working directory to use Pycharm console
# os.chdir("/Users/NikJM/Desktop/Uni/Python/examtse2020-21/Code")
# os.chdir("/Users/clemencebutin/PycharmProjects/examtse2020-21/Data")


importpath = os.path.abspath(("../Data/Coffeebar_2016-2020.csv"))
df = pd.read_csv(importpath, sep=";")
probs = pd.read_csv("../Results/Probs.csv", sep=";")
#New, uniform probabilities for last exercise
#for prob in range(probs.shape[0]):
#    DrinkUnif = np.repeat(1/6, 6)
#    FoodUnif = np.repeat(1/5, 5)
#    probs.iloc[prob, 1:7] = DrinkUnif
#    probs.iloc[prob, 7:12] = FoodUnif
print(probs)

####Create menu as dictionary: maps name of the item to its second argument (price)
menufood = {'sandwich': 2, 'cookie': 2, 'pie': 3, 'muffin': 3, 'none': 0}
menudrink = {'milkshake': 5, 'frappucino': 4, 'water': 2, 'coffee': 3, 'soda': 3, 'tea': 3}

# Create indexes for the time variable
index = pd.Index(probs['time'])


##################################################################
#############################Define classes#######################
##################################################################

class Customers(object):
    def __init__(self, ID):
        self.class_name = None
        self.ID = ID
        self.budget = 100
        self.time = None
        self.drink = 'none'
        self.food = 'none'
        self.pricefood = 0
        self.pricedrink = 0
        self.expense = 0
        self.inflation = 1

    def purchase(self, time, year, probs):
#        if year < 2023:
#            self.inflation = 1
#       elif year >= 2023:
#           self.inflation = 1.2
        if probs[probs['time'] == time]["none"].iloc[0] == 1:
            self.drink = np.random.choice(probs.columns[1:7], replace=True, p=probs.iloc[index.get_loc(time), 1:7])
            self.pricedrink = menudrink[self.drink] * self.inflation
            self.expense = self.pricedrink
            self.budget = self.budget - self.expense
        elif probs[probs['time'] == time]["none"].iloc[0] != 1:
            self.food = np.random.choice(probs.columns[7:12], replace=True, p=probs.iloc[index.get_loc(time), 7:12])
            self.pricefood = menufood[self.food] * self.inflation
            self.drink = np.random.choice(probs.columns[1:7], replace=True, p=probs.iloc[index.get_loc(time), 1:7])
            self.pricedrink = menudrink[self.drink] * self.inflation
            self.expense = round((self.pricedrink + self.pricefood), 2)
            self.budget = self.budget - self.expense

    # Create methods that apply to all customers
    def tell_drink(self, time):
        print("Ah! This %s at %s really hits the spot!" % (self.drink, time))

    def tell_food(self, time):
        if probs[probs['time'] == time]["none"].iloc[0] == 1:
            print("I was not that hungry though.")
        elif probs[probs['time'] == time]["none"].iloc[0] != 1:
            print("And it goes well with a %s." % (self.food))

    def tell_expense(self):
        print("The whole thing was %s bucks, but totally worth it!" % (self.expense))


class Returning(Customers):
    def __init__(self, ID):
        super().__init__(ID)
        # self.class_name = "Returning"
        self.food_history = []
        self.drink_history = []
        self.budget_history = []

    # Create method for Returning
    def purchase(self, time, year, probs):
#        if year < 2023:
#           self.inflation = 1
#       elif year >= 2023:
#           self.inflation = 1.2
        if probs[probs['time'] == time]["none"].iloc[0] == 1:
            self.drink = np.random.choice(probs.columns[1:7], replace=True, p=probs.iloc[index.get_loc(time), 1:7])
            self.pricedrink = menudrink[self.drink] * self.inflation
            self.expense = self.pricedrink
            self.budget = self.budget - self.pricedrink
            self.drink_history.append(self.drink)
            self.budget_history.append(self.budget)
        elif probs[probs['time'] == time]["none"].iloc[0] != 1:
            self.food = np.random.choice(probs.columns[7:12], replace=True, p=probs.iloc[index.get_loc(time), 7:12])
            self.pricefood = menufood[self.food] * self.inflation
            self.drink = np.random.choice(probs.columns[1:7], replace=True, p=probs.iloc[index.get_loc(time), 1:7])
            self.pricedrink = menudrink[self.drink] * self.inflation
            self.expense = round((self.pricedrink + self.pricefood), 2)
            self.budget = self.budget - self.expense
            self.food_history.append(self.food)
            self.drink_history.append(self.drink)
            self.budget_history.append(self.budget)


class Onetimers(Customers):
    def __init__(self, ID):
        super().__init__(ID)
        # self.class_name = "Onetimers"


class Returning_regular(Returning):
    def __init__(self, ID):
        super().__init__(ID)
        self.class_name = "Returning_regular"
        self.budget = 250


class Hipster(Returning):
    def __init__(self, ID):
        super().__init__(ID)
        self.class_name = "Hipster"
        self.budget = 500


class Tripadvisor(Onetimers):
    def __init__(self, ID):
        super().__init__(ID)
        self.class_name = "Tripadvisor"

    def give_tips(self):
        self.tip = round(uniform(0, 10), 2)
        self.expense = round(self.expense + self.tip, 2)


class Onetimer_regular(Onetimers):
    def __init__(self, ID):
        super().__init__(ID)
        self.class_name = "Onetimer_regular"
