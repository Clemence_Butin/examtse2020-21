########################################################
############# TSE M2 EEE: Python Programming ###########
############## Clémence Butin 21910436 #################
##############  Nikita Marini 21900525 #################
########################################################

import os
import numpy as np
import pandas as pd
import datetime
import matplotlib.pyplot as plt

# Declare working directory to use Pycharm console
# os.chdir("/Users/NikJM/Desktop/Uni/Python/examtse2020-21/Code")
# os.chdir("/Users/clemencebutin/PycharmProjects/examtse2020-21/Data")

# Import the original data
importpath = os.path.abspath(("../Data/Coffeebar_2016-2020.csv"))
df = pd.read_csv(importpath, sep=";")

print(df)
print(df.columns)

# Focus on the drinks and food variables
print(df["DRINKS"].describe())
print(df["FOOD"].describe())

print(df["DRINKS"].value_counts())  # 6 different types of drinks
print(df["FOOD"].value_counts())  # 4 different types of food
print(df["CUSTOMER"].value_counts())  # 247,988 unique customers

# Creation of a column just with the year
df['YEAR'] = df['TIME'].apply(lambda strtime: datetime.datetime.strptime(strtime, '%Y-%m-%d %H:%M:%S').year)
print(df)

# Count the number of items sold per each year
ItemsSold = df[['DRINKS', 'FOOD', 'YEAR']].groupby('YEAR').count()
print(ItemsSold)
# Plot items sold per year
ItemsSold.plot.bar(ylim=(20000, max(ItemsSold['DRINKS']) + 1000))
plt.title('Food and drinks bought per year')
plt.show()
# Plot only the drinks
ItemsSold.plot.bar(y='DRINKS')
plt.title('Drinks bought per year')
plt.show()
# Plot only the foods, with the y-bar starting at 32,000 so that we can see the difference
ItemsSold.plot.bar(y='FOOD', ylim=(32000, max(ItemsSold['FOOD']) + 100))
plt.title('Food bought per year')
plt.show()

#############################################################################
#############################################################################
###########################DETERMINE PROBABILITIES###########################
#############################################################################
#############################################################################

# Create hour-minute timestamp
df['TIMESTAMP'] = df['TIME'].apply(
    lambda strtime: str(datetime.datetime.strptime(strtime, '%Y-%m-%d %H:%M:%S').strftime('%H:%M')))

# Probability at 8h
df[df['TIMESTAMP'] == df['TIMESTAMP'].iloc[0]]['DRINKS'].value_counts() / np.sum(
    df[df['TIMESTAMP'] == df['TIMESTAMP'].iloc[0]]['DRINKS'].value_counts())

# Determine probabilities in a "manual" way
# for time in np.unique(df['TIMESTAMP']):
#    print(time)
#    print(df[df['TIMESTAMP'] == time]['DRINKS'].value_counts()/np.sum(df[df['TIMESTAMP'] == time]['DRINKS'].value_counts()))

# for time in np.unique(df['TIMESTAMP']):
#    print(time)
#    print(df[df['TIMESTAMP'] == time]['FOOD'].value_counts()/np.sum(df[df['TIMESTAMP'] == time]['FOOD'].value_counts()))

# Probability of each drink item at each time
ListDrinks = []
for time in np.unique(df['TIMESTAMP']):
    print(time)
    temp_df = df[df['TIMESTAMP'] == time]['DRINKS'].value_counts(normalize=True).rename(time)
    ListDrinks.append(temp_df)
pd.concat(ListDrinks, axis=1)

# Probability of each food item at each time
ListFood = []
for time in np.unique(df['TIMESTAMP']):
    print(time)
    temp_df = df[df['TIMESTAMP'] == time]['FOOD'].value_counts(normalize=True, dropna=False).rename(time)
    ListFood.append(temp_df)
pd.concat(ListFood, axis=1)

# Transform into dataframes
DrinksProb = pd.DataFrame(ListDrinks)
FoodProb = pd.DataFrame(ListFood)

# Merge them and clean it
probs = pd.merge(DrinksProb, FoodProb, how='left', left_index=True, right_index=True)
probs['time'] = probs.index
cols = probs.columns.tolist()
cols = cols[-1:] + cols[:-1]
probs = probs[cols]

# Redefine probability of not buying any food as (1-sum(other_probs))
probs = probs.drop(probs.columns[7], axis=1)
probs['none'] = (1 - probs.iloc[:, 7:].sum(axis=1))
# Transform nan into 0
probs.iloc[:, 7:] = probs.iloc[:, 7:].fillna(0)
print(probs)

# Show probability of each food/drink being bought (one graph for all the foods, one for all the drinks,
# with lines of different colors) at each time -- sandwiches for instance are only eaten at midday
# DRINKS
plt.plot(probs['time'], probs['coffee'], color='sienna')
plt.plot(probs['time'], probs['frappucino'], color='goldenrod')
plt.plot(probs['time'], probs['milkshake'], color='olive')
plt.plot(probs['time'], probs['water'], color='aqua')
plt.plot(probs['time'], probs['soda'], color='red')
plt.plot(probs['time'], probs['tea'], color='seagreen')
plt.xticks(probs['time'][::10], probs['time'][::10])
plt.title('Probabilities of purchase - Drinks')
plt.xlabel('Time of the day')
plt.ylabel('Probability')
plt.legend()
plt.show()

# FOOD
plt.plot(probs['time'], probs['cookie'], color='sienna')
plt.plot(probs['time'], probs['muffin'], color='goldenrod')
plt.plot(probs['time'], probs['pie'], color='olive')
plt.plot(probs['time'], probs['sandwich'], color='aqua')
plt.plot(probs['time'], probs['none'], color='black')
plt.xticks(probs['time'][::10], probs['time'][::10])
plt.title('Probabilities of purchase - Food')
plt.xlabel('Time of the day')
plt.ylabel('Probability')
plt.legend()
plt.show()

# Export probability table
#probs.to_csv("../Results/Probs.csv", sep=";", index=False)
